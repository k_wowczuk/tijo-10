import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ProductTest {
    private ShopService shop;
    private Product laptop = new Product("laptop",2999.99);
    private Product usb = new Product("usb",9.99);
    private Product mouse = new Product("mouse",99.99);

    @BeforeEach
    public void init(){
        shop = new Shop();
    }
    @Test
    public void checkCorrectOperationsForShoppingCart() {
        assertEquals(1, shop.addProductToBasket(laptop,2),"add laptop to basket");
        assertEquals(2, shop.addProductToBasket(usb,5),"add usb to basket");
        assertEquals(3, shop.addProductToBasket(mouse,1),"add mouse to basket");
        assertEquals(3, shop.addProductToBasket(laptop,1),"add laptop to basket");
        assertEquals(9149.91, shop.getTotalPrice(),"price total");
        assertEquals(3, shop.countProducts(),"count 3");
        assertEquals(2, shop.deleteProductFromBasket(mouse), "remove mouse from basket");
        assertEquals(1, shop.deleteProductFromBasket(laptop),"remove laptop from basket");
        assertEquals(49.95, shop.getTotalPrice(),"price total");
        assertEquals(1, shop.countProducts(),"count 1");
        assertEquals(49.95, shop.getPriceProduct(usb),"price usb from basket");
        assertEquals(9.99, shop.getPriceOneProduct(usb),"price one usb from basket");
    }
}
