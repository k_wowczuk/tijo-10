public interface ShopService {
    int addProductToBasket(Product product, int quantity);
    int deleteProductFromBasket(Product product);
    int countProducts();
    double getPriceOneProduct(Product product);
    double getPriceProduct(Product product);
    double getTotalPrice();
}