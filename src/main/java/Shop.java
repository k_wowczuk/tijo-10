import java.util.ArrayList;
import java.util.List;

public class Shop implements ShopService {
    private List<Basket> baskets = new ArrayList();

    public int addProductToBasket(Product product, int quantity) {
        boolean exist = false;
        double countPriceForProduct = product.getPriceProduct() * quantity;

        for (Basket b : baskets) {
            if (b.getProduct().equals(product)) {
                double newPrice = b.getPricesProduct() + countPriceForProduct;
                b.setPricesProduct(newPrice);
                b.setQuantity(b.getQuantity() + quantity);
                exist = true;
            }
        }

        if (!exist) {
            baskets.add(new Basket(product, quantity, countPriceForProduct));
        }
        return baskets.size();
    }

    public int deleteProductFromBasket(Product product) {
        int i = 0;
        for (Basket b : baskets) {
            if (b.getProduct().equals(product)) {
                i = baskets.indexOf(b);
            }
        }
        baskets.remove(i);
        return baskets.size();
    }

    public int countProducts() {
        if (baskets.size() == 0) {
            return 0;
        } else {
            return baskets.size();
        }
    }

    public double getPriceOneProduct(Product product) {
        for (Basket b : baskets) {
            if (b.getProduct().equals(product)) {
                return baskets.get(baskets.indexOf(b)).getProduct().getPriceProduct();

            }
        }
        return 0;
    }

    public double getPriceProduct(Product product) {
        for (Basket b : baskets) {
            if (b.getProduct().equals(product)) {
                return baskets.get(baskets.indexOf(b)).getPricesProduct();
            }
        }
        return 0;
    }

    public double getTotalPrice() {
        double total = 0;
        for (Basket b : baskets) {
            total += b.getPricesProduct();
        }
        return (double) Math.round(total * 100000d) / 100000d;
    }
}