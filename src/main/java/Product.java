public class Product {
    private String nameProduct;
    private double priceProduct;

    public Product(String nameProduct, double priceProduct) {
        this.nameProduct = nameProduct;
        this.priceProduct = priceProduct;
    }

    public double getPriceProduct() {
        return priceProduct;
    }
}