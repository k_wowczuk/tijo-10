public class Basket {
    private Product product;
    private int quantity;
    private double pricesProduct;

    public Basket(Product product, int quantity, double pricesProduct) {
        this.product = product;
        this.quantity = quantity;
        this.pricesProduct = pricesProduct;
    }

    public Product getProduct() {
        return product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPricesProduct() {
        return pricesProduct;
    }

    public void setPricesProduct(double pricesProduct) {
        this.pricesProduct = pricesProduct;
    }
}